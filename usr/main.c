/**
 * Copyright (c) 2011-2016, Mobangjack Ī��� (mobangjack@foxmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include "main.h"

void TestPC6789(void)
{
	GPIO_InitTypeDef gpio;
    
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
	
	gpio.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9;
	gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOC,&gpio);
	GPIO_SetBits(GPIOC, GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9);
}

int main(void)
{
	BSP_Config();
	//TestPC6789();
	while(1)
	{
		if(Micros() % 20000 == 0)
		{
			//GPIO_ToggleBits(GPIOC, GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9);
			//printf("ctrlMode=%d,claw1State=%d,pwm=%d\n",ctrlMode, claw1State, CLAW1_SERVO1_PWM);
			printf("ctrlMode=%d,claw1State=%d,claw2State=%d,PWM1=%d,PWM2=%d,PWM3=%d,PWM4=%d\n",ctrlMode, claw1State, claw2State, CLAW1_SERVO1_PWM, CLAW1_SERVO2_PWM, CLAW2_SERVO1_PWM, CLAW2_SERVO2_PWM);
		}
    }
}
