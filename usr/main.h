/**
 * Copyright (c) 2011-2016, Mobangjack Ī��� (mobangjack@foxmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#ifndef __MAIN_H__
#define __MAIN_H__

#include "stm32f4xx.h"

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>

//alg
#include "ramp.h"
#include "fifo.h"
#include "crc16.h"
#include "sprotocol.h"
#include "watchdog.h"

//bsp
#include "bsp.h"
#include "delay.h"
#include "led.h"
#include "pwm.h"
#include "timer.h"
#include "usart1.h"
#include "usart3.h"

//app
#include "InputTask.h"
#include "ControlTask.h"
#include "SuperviseTask.h"

//common definition
#define PI 3.1415926f
#define MAP(val,min1,max1,min2,max2) ((val-min1)*(max2-min2)/(max1-min1)+min2)

#endif 
