/**
 * Copyright (c) 2011-2016, Mobangjack Ī��� (mobangjack@foxmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include "main.h"

CtrlMode ctrlMode = CTRL_MODE_RC;
CtrlMode lastCtrlMode = CTRL_MODE_RC;
SwitchPanel switchPanel;
Keyboard keyboard;
uint8_t apiCmd;
ClawState claw1State = CLAW_STATE_OPEN;
ClawState claw2State = CLAW_STATE_OPEN;

void GetCtrlMode(void)
{
	if(Watchdog_IsLost(&watchdog[WATCHDOG_INDEX_RC]))
	{
		ctrlMode = CTRL_MODE_API;
	}
	else
	{
		if(dbus.rc.s2 == SW_UP)
		{
			ctrlMode = CTRL_MODE_RC;
		}
		else if(dbus.rc.s2 == SW_MD)
		{
			ctrlMode = CTRL_MODE_HC;
		}
		else if(dbus.rc.s2 == SW_DN)
		{
			ctrlMode = CTRL_MODE_API;
		}
	}
}

void GetSwitchState(SwitchState* s, uint8_t v)
{
	s->lastState = s->thisState;
	s->thisState = v;
	s->action = GET_SWITCH_ACTION(s->lastState, s->thisState);
}

void GetKeyAction(KeyState* k, uint8_t v)
{
	k->lastState = k->thisState;
	k->thisState = v;
	k->action = GET_KEY_ACTION(k->lastState, k->thisState);
}

void GetSwitchPanel(void)
{
	GetSwitchState(&switchPanel.s1, dbus.rc.s1);
	GetSwitchState(&switchPanel.s2, dbus.rc.s2);
}

void GetKeyboard(void)
{
	GetKeyAction(&keyboard.A, dbus.hc.key.val&KEY_A);
	GetKeyAction(&keyboard.D, dbus.hc.key.val&KEY_D);
	GetKeyAction(&keyboard.W, dbus.hc.key.val&KEY_W);
	GetKeyAction(&keyboard.S, dbus.hc.key.val&KEY_S);
	GetKeyAction(&keyboard.Shift, dbus.hc.key.val&KEY_SHIFT);
	GetKeyAction(&keyboard.Ctrl, dbus.hc.key.val&KEY_CTRL);
	GetKeyAction(&keyboard.Q, dbus.hc.key.val&KEY_Q);
	GetKeyAction(&keyboard.E, dbus.hc.key.val&KEY_E);
}

void GetRcClaw1State(void)
{
	if(switchPanel.s1.action == SWITCH_ACTION_3TO1)
	{
		if(claw1State == CLAW_STATE_CLOSE)
		{
			claw1State = CLAW_STATE_OPEN;
		}
		else if(claw1State == CLAW_STATE_OPEN)
		{
			claw1State = CLAW_STATE_CLOSE;
		}
	}
}

void GetRcClaw2State(void)
{
	if(switchPanel.s1.action == SWITCH_ACTION_3TO2)
	{
		if(claw2State == CLAW_STATE_CLOSE)
		{
			claw2State = CLAW_STATE_OPEN;
		}
		else if(claw2State == CLAW_STATE_OPEN)
		{
			claw2State = CLAW_STATE_CLOSE;
		}
	}
}

void GetHcClaw1State(void)
{
	if(keyboard.Q.action == KEY_ACTION_DN)
	{
		if(claw1State == CLAW_STATE_CLOSE)
		{
			claw1State = CLAW_STATE_OPEN;
		}
		else if(claw1State == CLAW_STATE_OPEN)
		{
			claw1State = CLAW_STATE_CLOSE;
		}
	}
}

void GetHcClaw2State(void)
{
	if(keyboard.E.action == KEY_ACTION_DN)
	{
		if(claw2State == CLAW_STATE_CLOSE)
		{
			claw2State = CLAW_STATE_OPEN;
		}
		else if(claw2State == CLAW_STATE_OPEN)
		{
			claw2State = CLAW_STATE_CLOSE;
		}
	}
}

void GetApiCmd(void)
{
	apiCmd = usart3_rx_data;
}

void GetApiClaw1State(void)
{
	if(apiCmd == API_CMD_CLOSE_CLAW1)
	{
		claw1State = CLAW_STATE_CLOSE;
	}
	else if(apiCmd == API_CMD_OPEN_CLAW1)
	{
		claw1State = CLAW_STATE_OPEN;
	}
}

void GetApiClaw2State(void)
{
	if(apiCmd == API_CMD_CLOSE_CLAW2)
	{
		claw2State = CLAW_STATE_CLOSE;
	}
	else if(apiCmd == API_CMD_OPEN_CLAW2)
	{
		claw2State = CLAW_STATE_OPEN;
	}
}

void InputTask(void)
{
	GetCtrlMode();
	if(ctrlMode == CTRL_MODE_RC)
	{
		GetSwitchPanel();
		GetRcClaw1State();
		GetRcClaw2State();
	}
	else if(ctrlMode == CTRL_MODE_HC)
	{
		GetKeyboard();
		GetHcClaw1State();
		GetHcClaw2State();
	}
	else if(ctrlMode == CTRL_MODE_API)
	{
		GetApiCmd();
		GetApiClaw1State();
		GetApiClaw2State();
	}
}
