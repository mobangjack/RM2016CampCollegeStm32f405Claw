/**
 * Copyright (c) 2011-2016, Mobangjack Ī��� (mobangjack@foxmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#ifndef __INPUT_TASK_H__
#define __INPUT_TASK_H__

#include <stdint.h>

#define GET_SWITCH_ACTION(lastState,thisState) (((lastState)<<2)|(thisState))
#define SWITCH_ACTION_3TO1 GET_SWITCH_ACTION(3, 1)
#define SWITCH_ACTION_1TO3 GET_SWITCH_ACTION(1, 3)
#define SWITCH_ACTION_3TO2 GET_SWITCH_ACTION(3, 2)
#define SWITCH_ACTION_2TO3 GET_SWITCH_ACTION(2, 3)
#define SWITCH_ACTION_NONE 0

#define GET_KEY_ACTION(lastState,thisState) (((lastState)<<1)|(thisState))
#define KEY_ACTION_UP GET_KEY_ACTION(KEY_DN, KEY_UP)
#define KEY_ACTION_DN GET_KEY_ACTION(KEY_UP, KEY_DN)

#define API_CMD_CLOSE_CLAW1 0x10
#define API_CMD_OPEN_CLAW1 	0x11
#define API_CMD_CLOSE_CLAW2 0x20
#define API_CMD_OPEN_CLAW2 	0x21

typedef enum
{
	CTRL_MODE_RC = 0,
	CTRL_MODE_HC = 1,
	CTRL_MODE_API = 2,
}CtrlMode;

typedef enum
{
	CLAW_STATE_CLOSE = 0,
	CLAW_STATE_OPEN = 1,
}ClawState;

typedef struct
{
	uint8_t thisState;
	uint8_t lastState;
	uint8_t action;
}SwitchState;

typedef struct
{
	uint8_t thisState;
	uint8_t lastState;
	uint8_t action;
}KeyState;

typedef struct
{
	SwitchState s1;
	SwitchState s2;
}SwitchPanel;

typedef struct
{
	KeyState A;
	KeyState D;
	KeyState W;
	KeyState S;
	KeyState Shift;
	KeyState Ctrl;
	KeyState Q;
	KeyState E;
}Keyboard;

void InputTask(void);

extern CtrlMode ctrlMode;
extern CtrlMode lastCtrlMode;
extern SwitchPanel switchPanel;
extern Keyboard keyboard;
extern uint8_t apiCmd;
extern ClawState claw1State;
extern ClawState claw2State;

#endif
