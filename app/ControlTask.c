/**
 * Copyright (c) 2011-2016, Mobangjack Ī��� (mobangjack@foxmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include "main.h"

void Claw1Open(void)
{
	CLAW1_SERVO1_PWM = ANGLE_TO_PWM(CLAW1_SERVO1_OPEN_ANGLE);
	CLAW1_SERVO2_PWM = ANGLE_TO_PWM(CLAW1_SERVO2_OPEN_ANGLE);
}

void Claw1Close(void)
{
	CLAW1_SERVO1_PWM = ANGLE_TO_PWM(CLAW1_SERVO1_CLOSE_ANGLE);
	CLAW1_SERVO2_PWM = ANGLE_TO_PWM(CLAW1_SERVO2_CLOSE_ANGLE);
}

void Claw2Open(void)
{
	CLAW2_SERVO1_PWM = ANGLE_TO_PWM(CLAW2_SERVO1_OPEN_ANGLE);
	CLAW2_SERVO2_PWM = ANGLE_TO_PWM(CLAW2_SERVO2_OPEN_ANGLE);
}

void Claw2Close(void)
{
	CLAW2_SERVO1_PWM = ANGLE_TO_PWM(CLAW2_SERVO1_CLOSE_ANGLE);
	CLAW2_SERVO2_PWM = ANGLE_TO_PWM(CLAW2_SERVO2_CLOSE_ANGLE);
}

void Claw1Control(void)
{
	if(claw1State == CLAW_STATE_CLOSE)
	{
		Claw1Close();
	}
	else if(claw1State == CLAW_STATE_OPEN)
	{
		Claw1Open();
	}
}

void Claw2Control(void)
{
	if(claw2State == CLAW_STATE_CLOSE)
	{
		Claw2Close();
	}
	else if(claw2State == CLAW_STATE_OPEN)
	{
		Claw2Open();
	}
}

static uint32_t ms_tick = 0;
void ControlTask(void)
{
	ms_tick++;
	if(ms_tick % 4 == 0)
	{
		SuperviseTask();
		Claw1Control();
		Claw2Control();
	}
}

