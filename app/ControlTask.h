/**
 * Copyright (c) 2011-2016, Mobangjack Ī��� (mobangjack@foxmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#ifndef __CONTROL_TASK_H__
#define __CONTROL_TASK_H__

#include <stdint.h>

#define CLAW1_SERVO1_PWM PWM1
#define CLAW1_SERVO2_PWM PWM2
#define CLAW2_SERVO1_PWM PWM3
#define CLAW2_SERVO2_PWM PWM4

#define ANGLE_TO_PWM(angle) (MAP(angle,0,180,500,2500))

#define CLAW1_SERVO1_OPEN_ANGLE 0
#define CLAW1_SERVO2_OPEN_ANGLE 180
#define CLAW1_SERVO1_CLOSE_ANGLE 90
#define CLAW1_SERVO2_CLOSE_ANGLE 90

#define CLAW2_SERVO1_OPEN_ANGLE 0
#define CLAW2_SERVO2_OPEN_ANGLE 180
#define CLAW2_SERVO1_CLOSE_ANGLE 90
#define CLAW2_SERVO2_CLOSE_ANGLE 90

void ControlTask(void);

#endif

