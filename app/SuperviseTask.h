#ifndef __SUPERVISE_TASK_H__
#define __SUPERVISE_TASK_H__

#include "watchdog.h"

#define WATCHDOG_NUM 2

#define WATCHDOG_INDEX_RC 0
#define WATCHDOG_INDEX_HC 1

#define WATCHDOG_COUNTER_RC 2000
#define WATCHDOG_COUNTER_HC 2000

void SuperviseTask(void);

extern Watchdog watchdog[WATCHDOG_NUM];

#endif

