#include "main.h"

Watchdog watchdog[WATCHDOG_NUM] = {2000, 2000};

uint32_t lostError = 0xffffffff;

void SuperviseTask(void)
{
	static uint32_t i = 0;
	for(i = 0; i < WATCHDOG_NUM; i++)
	{
		if(!Watchdog_IsLost(&watchdog[i]))
		{
			Watchdog_Count(&watchdog[i]);
			lostError &= ~(uint32_t)(1 << i);
		}
		else
		{
			lostError |= (uint32_t)(1 << i);
		}
	}
}
